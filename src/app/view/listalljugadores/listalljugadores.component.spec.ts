import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListalljugadoresComponent } from './listalljugadores.component';

describe('ListalljugadoresComponent', () => {
  let component: ListalljugadoresComponent;
  let fixture: ComponentFixture<ListalljugadoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListalljugadoresComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListalljugadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
