import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect.service";
import {Jugadors} from "../../shared/classes/Jugadors";

@Component({
  selector: 'app-listalljugadores',
  templateUrl: './listalljugadores.component.html',
  styleUrls: ['./listalljugadores.component.css']
})
export class ListalljugadoresComponent {
  message!:string;
  jugadors !: Jugadors[];
  constructor(private connectbd: ConnectService){};
  resposta() {
    this.connectbd.getAllJugadores().subscribe(res => {
      this.message = "";

      if (res.length > 0){
        for(let i = 0; i < res.length; i++){
          this.message += "<p>Id: " + res[i].jugId + ", name: " + res[i].nombre
            + ", apellidos: " + res[i].apellidos + "<p/>";

        }

      } else {
        this.message = "<p>No hi ha elements.</p>";
      }
    });
  }
}
