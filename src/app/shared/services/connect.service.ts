import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ConnectService {

 REST_API: string = 'http://localhost:9000';
  constructor(private httpclient : HttpClient) {}
  public getAlive(): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/`);
  }
  public getAllJugadores(): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/allJugadors`);
  }
}
